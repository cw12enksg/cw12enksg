import random as rd


def addition(n, p):
    # n = nb à décomposer
    # p = nombres opérations
    if p == 1:
        return str(n)
    else:
        a = rd.random()
        b = rd.randint(1, (n + 1 - p) // 2)
        if a < 0.7:
            return str(b) + '+' + addition(n - b, p - 1)
        else:
            return str(b) + '+' + multiplication(n - b, p - 1)


def multiplication(n, p):
    if p == 1:
        return str(n)
    else:
        a = rd.random()
        l = []
        for i in range(1, n // 2 + 1):
            if n % i == 0:
                l.append(i)
        rd.shuffle(l)
        b = l[0]
        if a < 0.8:
            return str(b) + '*' + addition(n // b, p - 1)
        else:
            return str(b) + '*' + multiplication(n // b, p - 1)
