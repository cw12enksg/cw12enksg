import tkinter
from tkinter import *
from mathmaze.grid_graph import *
from mathmaze.display_qcm import *

def launch_qcm(): #lance le qcm quand on appuie sur le bouton
    global menu
    menu.destroy()
    graphical_QCM_init()


def launch_maze(): # lance le labyrinthe quand on appuie sur le bouton
    global menu
    menu.destroy()
    graphical_grid(7)


def quit_menu(): #quitte le menu (et donc le jeu)
    menu.destroy()


#crée la fenêtre du menu
menu = Tk()
menu.geometry('300x300')
menu.title('Mathmaze')
text_menu = Label(menu,text = 'Menu', font=('Times',40))
text_menu.pack()
play_qcm_button = Button(menu,text = 'Nouvelle partie en mode QCM', command = launch_qcm)
play_qcm_button.pack()
play_maze_button = Button(menu, text = 'Nouvelle partie en mode labyrinthe', command = launch_maze)
play_maze_button.pack()
quit_menu_button = Button(menu, text =' Quitter', command = quit_menu)
quit_menu_button.pack()
menu.mainloop()
