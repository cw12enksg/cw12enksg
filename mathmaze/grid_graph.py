from tkinter import *
from mathmaze import grid_maze
from mathmaze import random_grid_v2 as rg2
from pygame import *
from pygame.mixer import Sound

COMMANDS = {"Up": "up", "Left": "left", "Right": "right", "Down": "down"}


def graphical_grid(taille,grid_not_created = True,grille_depart=[],depart=[],fin='',sortie=''): #interface tkinter
    global size
    size = taille

    def update_graphical_grid(grid, player_position, list_cell): #met à jour les labels de chaque frame
        global size
        n = size
        for i in range(n):
            for j in range(n):
                cellvalue = list_cell[i][j]
                if grid[i][j] == '-1':
                    cellvalue.config(background='black')
                elif [i, j] == player_position:
                    cellvalue.config(text=str(somme),background='cyan')
                elif (i, j) == player_position:
                    cellvalue.config(background='cyan')
                elif grid[i][j] == '0':
                    cellvalue.config(background='white',text='')
                elif grid[i][j] == 'e':
                    cellvalue.config(background='yellow',text='')
                elif grid[i][j] == 's':
                    cellvalue.config(text=sortie, background='green')
                else:
                    cellvalue.config(text=grid[i][j],background='white')
        if player_position == list(fin):
            if somme==sortie:
                #showinfo('Game won', 'Congratulation ! You win ! :p',)
                mixer.init()
                mario_victoire = Sound("niveau-termine.wav")
                mario_victoire.play()
                you_win(game)
            else:
                #showinfo('Game loose', 'Sorry, you lose. Try again ! :p')
                mixer.init()
                mario_defaite = Sound("game-over.wav")
                mario_defaite.play()
                you_lose(game,grille_depart,depart,fin,sortie)
        game.update_idletasks()

    def key_pressed(event): # lorsque l'on appuie sur une touche du clavier
        global grid, player_position
        global list_cell
        global somme
        key = event.keysym
        if key in COMMANDS:
            grid, player_position, somme = grid_maze.deplacement(grid, player_position, COMMANDS[key], somme)
            update_graphical_grid(grid, player_position, list_cell)

    def you_win(game): #se lance lorsque l'on gagne et affiche une fenêtre
        next_game_window = Toplevel(game)
        next_game_label = Label(next_game_window,text = 'You win. Wanna play again ?')
        next_game_label.pack()
        play_again_button = Button(next_game_window, text= 'Play again.', command = lambda:play_again(game))
        play_again_button.pack()
        leave_button = Button(next_game_window, text = 'Quit.',command=lambda:leave(game))
        leave_button.pack()

    def you_lose(game,grille_depart,depart,fin,sortie): #se lance quand on perd
        next_game_window = Toplevel(game)
        next_game_label = Label(next_game_window,text = 'You lose. Wanna play again ?')
        next_game_label.pack()
        play_again_button = Button(next_game_window, text= 'Play again.', command =lambda:play_again(game))
        play_again_button.pack()
        leave_button = Button(next_game_window, text = 'Quit.',command=lambda:leave(root))
        leave_button.pack()
        retry_button = Button(next_game_window, text = 'Retry.', command=lambda: retry(game,grille_depart,depart,fin,sortie))
        retry_button.pack()


    def leave(root):
        root.destroy()

    def play_again(game):
        global size
        root.destroy()
        graphical_grid(size)

    def retry(game,grille_depart,depart,fin,sortie):
        root.destroy()
        print(grille_depart)
        graphical_grid(size,False,grille_depart,depart,fin,sortie)

    global grid, player_position
    global list_cell
    global somme
    # grid =[['-1','-1','0','3','2'],['-1','6','0','-1','4'],['-1','5','-1','-1','6'],['-1','0','5','-1','4'],['0','0','0','0','0']]

    while grid_not_created:
        try :
            grille_depart, depart, fin , sortie = rg2.random_grid(size)
            grid_not_created = False
        except ValueError:
            grid_not_created = True
    player_position = depart
    grid = [[case for case in line] for line in grille_depart]
    root = Tk()
    game = Toplevel(root)
    game.bind("<Key>", key_pressed)
    background = Frame(game, bg='#9e948a')

    def launch_with_new_size(game,root,size_spinbox):
        size = size_spinbox.get()
        try :
            size = int(size)
            root.destroy()
            graphical_grid(size)
        except ValueError:
            print('''la valeur n'est pas correcte''' )

    #initialisation de la fenêtre paramètre
    root.title('Settings')
    label_size=Label(root,text='Choose the size of the maze')
    label_size.pack()
    size_spinbox=Spinbox(root, from_=5, to=40, increment=1)
    size_spinbox.pack()
    set_size_button=Button(root, text= 'Set size', command = lambda:launch_with_new_size(game,root,size_spinbox))
    set_size_button.pack()


    #initialisation des cellules
    list_cell=[]
    for i in range(size): #initialise les frames
        graphical_line = []
        for j in range(size):
            cell = Frame(background, bg = 'white')
            cell.grid(row = i, column = j, padx = 1, pady = 1)
            label = Label(master = cell, text = ' ',bg = 'white',
                          justify = CENTER, font = {"Verdana", 40, "bold"},
                          width = 40//size, height= 20//size )
            label.grid()
            graphical_line.append(label)
        list_cell.append(graphical_line)
    background.grid()

    update_graphical_grid(grid, player_position, list_cell)
    somme = 0
    game.mainloop()


#graphical_grid()
