import random


def grid1():
    return [["-1", "-1", "0", "0", "0"], ["-1", "0", "0", "-1", "0"], ["-1", "0", "-1", "-1", "0"],
            ["-1", "0", "0", "-1", "0"], ["-1", "-1", "0", "0", "0"]]


DIRECTIONS = {"right": [0, 1], "left": [0, -1], "up": [-1, 0], "down": [1, 0]}



def deplacement(grid, player_position, d, somme):
    # d = "right","left","down" ou "up
    # pos=[i,j]= position actuelle
    size = len(grid)
    i = player_position[0]
    j = player_position[1]
    if isinstance(grid[i][j], int):
        x = int(grid[i][j])
    else:
        x = 0
    i2 = i + DIRECTIONS[d][0]
    j2 = j + DIRECTIONS[d][1]
    if 0 <= i2 < size and j2 >= 0 and j2 < size:
        if grid[i2][j2] in ['e', 's']:
            player_position = [i2, j2]
            grid[i][j]='0' # efface la valeur de la case précédente
        elif int(grid[i2][j2]) >= 0:
            grid[i2][j2] = str(int(grid[i2][j2]) + x)
            somme += int(grid[i2][j2])
            if grid[i][j] not in ['e', 's']:
                grid[i][j] = "0"
            player_position = [i2, j2]
    return grid, player_position, somme



