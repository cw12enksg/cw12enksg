import random as rd

def decomp(number, nb_op):
    '''
    Décompose en addition
    :param number: (int) Le nombre à décomposer
    :param nb_op: (int) Le nombre d'operation
    :return: (str) La décompostion du nombre en nb_op additions
    '''
    if nb_op == 1:
        return str(number)
    else:
        b = rd.randint(1, (number + 1 - nb_op) // 2)
        return str(b) + '+' + decomp(number - b, nb_op - 1)

def false_decomp(true_decomp):
    '''
    Donne une fausse opération dérivée de l'opération intiale
    :param true_decomp: (str) L'operation véritable initiale
    :return: (list) La liste des différents nombres de l'opération (addition) faussée
    '''
    f_decomp = true_decomp.split('+') # renvoie une liste des différents nombres de l'opération véritable
    i = rd.randint(0,len(f_decomp)-1) # prend un nombre aléatoire sur la longueur de la liste initiale
    k = rd.randint(0,2)               # cas de "déformation" de l'opération
    if k == 0:                                          # on change un élément aléatoirement
        f_decomp[i] = str(int(f_decomp[i]) + rd.randint(1,3))
    elif k == 1:                                        # on ajoute un ou plusieurs nombres
        for j in range(0,rd.randint(2,3)):
            f_decomp = f_decomp + [str(rd.randint(1,10))]
    else :                                              # on supprime un élément aléatoiremenent
        del f_decomp[i]
    return f_decomp

def read_player_guess(nb_decomp):
    '''
    Demande au joueur son choix de décomposition
    :param: (int) Le nombre de choix de décompostion
    :return: (str) L'index de la décompostion choisie
    '''
    user_guess = input('Enter your guess : ')
    while user_guess not in [str(i) for i in range(0,nb_decomp)]:
        print('''Incorrect command, please enter a new one !''')
        user_guess = input('Enter your guess : ')
    return user_guess

def ask_user():
    ''' Affiche le questionnaire '''

    number = rd.randint(10,100)                 # nombre à retrouver
    nb_op = rd.randint(2,10)                    # nombre d'operateur
    nb_decomp = rd.randint(2,4)                 # nombre de choix
    true_decomp = decomp(number,nb_op)    # opération de base 'la vraie'

    decomps = {}
    rand_i = rd.randint(0,nb_decomp)
    for i in range(0,nb_decomp):
        if i  == rand_i:
            decomps[i] = true_decomp            # la choix qui correspond à la vraie réponse
        else :
            unique_f_decomp_call = '+'.join(false_decomp(true_decomp))
            while unique_f_decomp_call  in decomps.values() :   # boucle pour éviter les fausses opérations identiques
                unique_f_decomp_call = '+'.join(false_decomp(true_decomp))
            decomps[i] = unique_f_decomp_call

    print('The number to find is : ', number)

    for i in range(len(decomps)):
        print("If it's the anwser -> ", decomps[i],",tape", i)

    user_guess = read_player_guess(nb_decomp)
    if user_guess == str(rand_i):
        print('You guessed right !')
    else :
        print('You guessed wrong !')

#ask_user()
