from mathmaze.grid_maze import *

grid = [["-1", "-1", "0", "0", "0"], ["-1", "0", "0", "-1", "0"], ["-1", "0", "-1", "-1", "0"],
            ["-1", "0", "0", "-1", "0"], ["-1", "-1", "0", "0", "0"]]

DIRECTIONS = {"right": [0, 1], "left": [0, -1], "up": [-1, 0], "down": [1, 0]}


def test_deplacement():
    assert deplacement(grid,[0,4],'right',1) == (grid,[0,4],1)
    assert deplacement(grid,[0,4],'down',1) == (grid,[1,4],1)
