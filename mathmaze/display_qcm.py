from tkinter import *
from tkinter.messagebox import *
from mathmaze.QCM import *

def graphical_QCM_init():

    def evaluate() :     # command pour un bouton pour vérifier si le choix est bon ou non
        guess = liste_answers.index(liste_answers.curselection())
        if guess == rand_i:
            showinfo('TRUE', 'You guessed right! :p')
            create_decomp()
            liste_answers.delete(0,END)
            for i in range(len(decomps)):                      # relance un autre QCM si le choix était le bon
                liste_answers.insert(i, decomps[i])
            liste_answers.configure(width= len(max(decomps.values()))+13, height= len(decomps)+1 )
            message_number = 'Le nombre à retrouver est : {}'.format(number)
            label_answers.configure(text = message_number)

        else :
            showinfo('FALSE', 'You guessed wrong! :p')

    def create_decomp():    # utilise les fonctions de QCM pour créer les decompositions (vraie et fausses)
        global rand_i, number, decomps
        number = rd.randint(10,100)                 # nombre à retrouver
        nb_op = rd.randint(2,20)                    # nombre d'operateur
        nb_decomp = rd.randint(2,3)                 # nombre de choix
        true_decomp = decomp(number,nb_op)    # décompostion de base 'la vraie'

        decomps = {}
        rand_i = rd.randint(0,nb_decomp)
        for i in range(0,nb_decomp+1):
            if i  == rand_i:
                decomps[i] = true_decomp            # la choix qui correspond à la vraie réponse
            else :
                unique_f_decomp_call = '+'.join(false_decomp(true_decomp))
                while unique_f_decomp_call  in decomps.values() :   # boucle pour éviter les fausses décompostions identiques
                    unique_f_decomp_call = '+'.join(false_decomp(true_decomp))
                decomps[i] = unique_f_decomp_call

    global rand_i, number, decomps
    root = Tk()
    root.title('Question mathématique ')

    create_decomp()                                  # creation des choix

    message_number = 'Le nombre à retrouver est : {}'.format(number)
    label_answers = LabelFrame(root, text = message_number)          # affichage de la liste des choix
    liste_answers = Listbox(label_answers, width= len(max(decomps.values()))+13, height= len(decomps)+1)

    for i in range(len(decomps)):
        liste_answers.insert(i, decomps[i])

    label_answers.pack()
    liste_answers.pack(fill=BOTH, expand=TRUE)

    quit_button = Button(root, text='Quitter', command = root.quit)   # bouton pour quitter le jeu
    quit_button.pack(side='left')

    guess_button = Button(root, text='Evaluer', command = evaluate)   # bouton pour évaluer la réponse choisie
    guess_button.pack(side='right')

    root.mainloop()

# if __name__=='__main__':
#     graphical_QCM_init()
#     exit(1)
