from mathmaze.QCM import *
from pytest import *

def test_decomp():
    ''' Test de la décompostion d'un nombre en addition '''
    decomposition = decomp(10,5)
    decomposition = decomposition.split('+')
    decomposition = [int(i) for i in decomposition]
    assert sum(decomposition) == 10

    decomposition = decomp(7,1)
    decomposition = int(decomposition)
    assert decomposition == 7

def test_false_decomp():
    ''' Test de la création d'une fausse décomposition à partir de la vraie décomposition '''
    t_decomp = decomp(10,5)
    f_decomp = false_decomp(t_decomp)
    f_decomp = [int(i) for i in f_decomp]
    t_decomp = t_decomp.split('+')
    assert sum(f_decomp) != 10
    l = len(t_decomp)
    assert len(f_decomp) == l or l+2 or l+3 or l-1

def test_read_player_guess(monkeypatch):
    ''' Test de l'input de l'utilisateur '''
    nb_decomp = 3
    results = '0'
    def mock_input_return(request):
        return results
    monkeypatch.setattr('builtins.input',mock_input_return)
    assert read_player_guess(nb_decomp) == results
