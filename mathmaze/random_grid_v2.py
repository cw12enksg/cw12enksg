import random as rd
from mathmaze import QCM

def empty_grid(size):
    '''
    Crée une grille de jeu vide (remplie de '0')  de taille NxN
    :param size: (int) La taille de la grille NxN (N = size)
    :return: (list) La grille de jeu vide (liste de listes)
    '''
    return [['0' for i in range(size)] for j in range(size)]

def get_possible_adjacents_tiles(size, center_tile, main_path):
    '''
    Donne les positions des cases possibles pour un chemin
    et qui sont adjacentes à une case de référence de postion (i,j)
    (Un chemin ne peut pas descendre)
    :param size: (int) La taille de la grille NxN (N = size)
    :param center_tile: (tuple) La position (i,j) de la case de référence
    :param main_path: (list) Le chemin principal (liste de positions de case (i,j))
    :return: (list) Les positions des cases adjacentes selon l'abscisse (i.e. selon j)
    '''
    i, j = center_tile                    # case de référence
    list_adj = []
    if j > 0 and (i, j - 1) not in main_path:   # case de réf non au bord gauche
        list_adj.append((i, j - 1))             # et case à gauche n'appartient pas au chemin
    if j < size - 1 and (i, j + 1) not in main_path:    # case de réf non au bord droit
        list_adj.append((i, j + 1))                     # et case à droite n'appartient pas au chemin
    return list_adj

def create_main_path(grid):
    '''
    Crée le chemin principal
    :param grid: (list) La grille de jeu (liste de listes)
    :return: (list) Le chemin principal (liste de position de case (i,j))
    '''
    size = len(grid)
    length = rd.randint(size + 2, 2 * size)                     # chosit la longueur du chemin
    climbing_steps = rd.sample(range(0, length), size - 1)      # le chemin va de la ligne du bas à la ligne du haut
                                                                # comme il n'y a pas descente il y a donc size-1 montées
    abs_start = rd.randint(0, size - 1)
    start = (size - 1, abs_start)                   # le chemin commence quelque part sur la dernière ligne de la grille
    main_path = [start]
    for step in range(0, length):
        i, j = main_path[-1]                    # pose une case de référence (la dernière position du chemin)
        if step in climbing_steps:              # si l'étape est une étape de montée donc on ajoute la case du haut au chemin
            main_path.append((i - 1, j))
        else:                                   # sinon on regarde les cases adjacentes
            dir_possible = get_possible_adjacents_tiles(size, (i, j), main_path)
            if len(dir_possible) == 2:                          # les deux côtés sont possibles,
                next_center = dir_possible[rd.randint(0, 1)]    # donc on en ajoute un aléatoirement au chemin
                main_path.append(next_center)
            elif len(dir_possible) == 1:                        # un seul côté est possible,
                next_center = dir_possible[0]                   # donc on l'ajoute au chemin
                main_path.append(next_center)
            elif len(dir_possible) == 0:                        # pas de côté possible
                return create_main_path(grid)                   # on recommence un chemin principal
    #print(main_path)
    return main_path

def fit_decomposition(grid, main_path, decomp):
    '''
    Etale la decomposition d'un nombre dans les cases du chemin principale
    :param grid: (list) La grille de jeu (liste de listes)
    :param main_path: (list) Le chemin principal (liste de position de case (i,j))
    :param decomposition: (list) La decomposition en operation d'un nombre (liste de strings)
    :return: (list) La grille de jeu avec une décompostion ajustée au chemin principale (liste de listes)
    '''
    length = len(main_path)
    len_decomp = len(decomp)

    to_fill = rd.sample(range(1, length - 1), len_decomp) # ne rempli que certaine case choisie aléatoirement
    for k in range(len_decomp):                           # ne rempli pas les extrémités
        i, j = main_path[to_fill[k]]
        grid[i][j] = decomp[k]
    i, j = main_path[0]       # départ du chemin
    grid[i][j] = 'e'
    i, j = main_path[-1]      # arrivée du chemin
    grid[i][j] = 's'
    return grid

def random_fill(grid, main_path, exit_value, pc_empty=0.1):
    '''
    Remplit aléatoirement la grille de mur, de case vide et de nombre
    :param grid: (list) La grille de jeu (liste de listes)
    :param main_path: (list) Le chemin principal (liste de position de case (i,j))
    :param exit_value: (int) La value de sortie (i.e. le nombre utilisé dans pour la décomposition)
    :param pc_wall: (float) Le pourcentage de case vide à transformer en mur (prédéfini à 30%)
    :param pc_empty: (float) Le pourcentage de case à lasser vide (prédefini à 10%)
    :return: (list) La grille de jeu rempli aléatoirement en dehors du chemin (liste de listes)
    '''
    pc_wall=0.3
    size = len(grid)
    for i in range(0, size):
        for j in range(0, size):
            test = rd.random()
            if (i, j) not in main_path:  # les cases qui ne sont pas dans le chemin principal
                if test < pc_wall:       # le test est vérifié, on crée un mur par les cases vides
                    grid[i][j] = '-1'
                elif test < pc_wall + pc_empty:  # parmi les cases vides restantes, on les laisse vides,
                    grid[i][j] = '0'             # si le test est vérifié
                else:
                    grid[i][j] = str(rd.randint(1, exit_value//2))  # sinon on remplit d'un nombre compris en 0 et la valeur à retrouver
    return grid

def random_grid(size):
    '''
    Crée une grille de jeu de taille NxN comprenant un chemin avec sa décompostion ajustée et un remplissage du reste
    :param size: (int) La taille de la grille NxN (N = size)
    :return: (list) La grille de jeu rempli aléatoirement (liste de listes)
    '''
    grid = empty_grid(size)                             # Grille initiale vide
    main_path = create_main_path(grid)             # Constitue le chemin principal

    start = main_path[0]                # début du chemin
    ending = main_path[-1]              # fin du chemin

    nb_op = rd.randint(size//2,size)                    # nombre d'operateur (aléatoire entre 2 et 20)
    exit_value = rd.randint(10,100)             # nombre à retrouver (aléatoire entre 10 et 100)
    decomposition = QCM.decomp(exit_value, nb_op)    # décompose sous la forme exit_value = x+y+z
    decomp = decomposition.split('+')               # decompose sous la forme ['x', 'y', 'z']
    while len(main_path)-2 < len(decomp):           # le nombre de case que prend la décompostion doit être inférieur
        exit_value = rd.randint(10,100)
        decomposition = QCM.decomp(exit_value, nb_op)
        decomp = decomposition.split('+')
    #print(decomp)
    grid = fit_decomposition(grid,main_path,decomp)     # decompostion ajusté dans le chemin
    grid = random_fill(grid,main_path,exit_value,1-len(decomposition)/len(main_path))  # remplissage des cases hors chemin
    return grid, start, ending, exit_value
