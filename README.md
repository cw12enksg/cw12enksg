# CODING WEEK GROUP 12 - Des jeux mathématiques made in CentraleSupélec

L'objectif de ce projet de la semaine 2 est de construire un (ou plusieurs) jeu(x) mathématique(s) destiné(s) aux élèves du primaire, en réutilisant les apprentissages de la semaine 1 pendant laquelle nous avons programmé de manière progressive un jeu 2048.
Le groupe est consititué des 5 membres suivants : 
* Eric TCHINDA KENNE 
* Nicolas RISSE
* Kim-Anh KAUFFMAN-TRAN THANH TAM 
* Stéphane WANG
* Guilhem GACHELIN

## À propos de notre jeu

Nous souhaitons codé une sorte de labyrinthe (maze en anglais) où des nombres des des opérateurs sont éparpillés dans une grille.

Voici la description de notre projet (suite à un brainstorming d'idées) : 

A partir d'un **Nombre**, une décomposition de nombres et d'opérateurs est crée. 
Ensuite, on altère cette décomposition pour obtenir des "fausses" décomposition. 
Puis on affiche le Nombre et les différentes décompositions (la véritable et les fausses); 
l'utilisateur entre un chiffre correspondant au choix de décompositions.

A partir de ce MVP, nous créons un labyrinthe où les chemins sont les décompositions (non rectiligne et avec des murs).
Avec les touches du clavier, le joueur se déplace dans le labyrinthe.
L'opérateur d'addition (+) n'est pas affiché, deux nombres consécutifs s'additionnent.
Les autres opérateurs (x , - , /) sont affichés.
L'objectif est de retrouver **Nombre** en réeffectuant la bonne décomposition de calcul.

Pour aller plus loin, nous pourrions créer des pièges, des cases spéciales ou une intéraction lors des carrefours dans le labyrinthe.

Notre jeu se nommera : **MATH'MAZE**

## Objectif 1 (MVP) : MATH'MAZE n'est pas encore vraiment un labyrinthe mais plutôt un questionnaire ?! (JOUR 1 et JOUR 2) 

## Sprint 0: 

### Analyse du problème

* Permettra de décomposer un nombre en une opération
* Créera d’autres opérations « fausses »
* Permettra à l’utilisateur d’essayer de trouver la solution
* Permettra à l’utilisateur de choisir les solutions
* Permettra de l’afficher sous forme de labyrinthe
* Permettra au joueur de se déplacer avec le clavier
* Fera évoluer la valeur du joueur dans la grille en fonction des déplacements
* Testera la solution du joueur

Commentaire en cours du projet :  
* En prenant en compte la difficulté de passage des points 1 à 4 au reste, les points 1 à 4 sont devenus, lors du projet, un "jeu" **QCM**. En utilisant le point 1 et le reste des points, nous avons crée le jeu **MATH'MAZE**. 
* Les opérateurs autre que l'addition sont plutôt difficile à implémenter dans le jeu, donc pour avoir quelque chose qui est jouable rapidement, nous nous sommes focalisé sur l'addistion unique pour le moement.

### Réflexion autour de la conception

En tant qu’utilisateur je veux pouvoir initier un jeu et jouer avec mon clavier
En tant que joueur je veux pouvoir me déplacer dans le labyrinthe et interagir avec les cases
En tant que développeur je veux pouvoir vérifier la solution choisie par le joueur

## Sprint 1: Mise en place des données du jeu

### Fonctionnalité 1 : Créer une décomposition d'un nombre

Permettra de décomposer un nombre en une opération (addition pour le moment).
Permettra de créer une opération "fausses" à partir de la décompostion de départ.

Codé par : **Eric et Stéphane**

### Fonctionnalité 2 : Questionnaire à choix multiple (i.e. affiche d'une interface de choix)

Créera plusieurs opérations "fausses".

Dans la console, l'utilisateur : 
Verra afficher :
* le **Nombre de départ**.
* la décompostion du **Nombre**
* les "fausses" décompositions

Devra choisir quelle opération donne le résultat voulu parmi plusieurs opérations générées aléatoirement (une seule sera valide).

Vérifiera la solution du joueur.

Codé par : **Eric et Stéphane**

Commentaire : Ici, un premier jeu est conçu, en vue des difficultés rencontrées. La suite de reprendra que le début de la fonctionnalité 1.


## Objectif 2 : MATH'MAZE ressemble enfin à un labyrinthe (Amélioration du MVP) (JOUR 1, JOUR 2 et JOUR 3)

## Sprint 2: Transposition de la décompostion à un labyrinthe

### Fonctionnalité 3 : Génération de grille aléatoire

Créera une grille de jeu vide.
Permettra de créer un chemin principale à partir d'une décomposition d'**un nombre aléatoire**.
Initialisera une entrée et une sortie.
Intégrera le chemin à la grille de jeu.
Remplira le reste de la grille avec des murs, des cases vides ou des nombres aléatoire.

Dans la grille, '0' correspond à une case vide, 'chiffre' à une case avec le chiffre, 'e' à l'entrée, 's' à la sortie.

Codé par : **Guilhem**

## Sprint 3: Gestions des déplacements

### Fonctionnalité 4 : Gestion d'un déplacement et critère de fin de jeu 

Permettra à une case de se déplacer dans le grille.
Permettra de bloquer les mouvements dans la direction d'un mur.
Créera une somme qui à l'arrivée permettra de vérifier la victoire ou la défaite.

Codé par : **Kim-Anh**

## Sprint 4: Affichage dans l'interface graphique de la grille, prise en compte des actions du joueur:

### Fonctionnalité 5 : Interface du QCM
Cééra une interface Tkinter.
Permettra à l’utilisateur d’essayer de trouver la solution.
Permettra à l’utilisateur de choisir parmi les solutions.
Affichera un message de "victoire" ou de "défaite".
Permettra de resélectionner un solution en cas de défaite.
Relancera une autre question en cas de victoire.

Codé par : **Stéphane

### Fonctionnalité 6 : Interface du labyrinth 
Créera une interface Tkinter.
Permettra de l’afficher la grille aléatoire (le labyrinthe MATH'MAZE).
Permettra au joueur de se déplacer avec le clavier.
Fera évoluer la valeur du joueur dans la grille en fonction des déplacements.
Testera la solution du joueur à l'arrivée.

Codé par : **Nicolas et Kim-Anh**

## Objectif 3 : MATH'MAZE est-il parfait? (JOUR 4)

## Sprint 5: Amélioration de l'interface graphique et fonctionnalités annexes:

### Fonctionnalité 7 : Menu de choix de jeu
Permettra à l'utilisateur de choisir certains paramètres via un menu (QCM ou labyrinthe, taille de la grille).
Affichera un message de victoire ou de défaite suivant le test de la solution.
Proposera à l'utilisateur de recommencer s'il a perdu.
Ajoutera une musique de victoire.
(Affichera le chemin parcouru par le joueur)

Codé par : **Eric, Guilhem et Stéphane**

## Sprint 6: La suite...


